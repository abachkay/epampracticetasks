﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    class Program
    {
        static void Main(string[] args)
        {                 
            var firstVector = new Vector(5);
            var secondVector = new Vector(2, 5);
            var thirdVector = new Vector(5, 10);
            var fourthVector = new Vector(5, 10);
            for (int i = 0; i < firstVector.Length; i++)
            {
                firstVector[i] = i;
            }
            for (int i = secondVector.LeftBoundary; i <= secondVector.RightBoundary; i++)
            {
                secondVector[i] = i;
            }
            for (int i = thirdVector.LeftBoundary; i <= thirdVector.RightBoundary; i++)
            {
                thirdVector[i] = i;
                fourthVector[i] = i+1;
            }
            
            Console.WriteLine("Trying to access element out of range");
            try
            {
                var element = secondVector[0];
            }
            catch(IndexOutOfRangeException exception)
            {
                Console.WriteLine($"Error: {exception.Message}");
            }
            
            Console.WriteLine("Trying to add vectors with different index boundaries");
            try
            {
                var fifthVector = secondVector.AddElementByElement(fourthVector);
            }
            catch(ArgumentException exception)
            {
                Console.WriteLine($"Error: {exception.Message}");
            }
            
            var sixthVector = thirdVector.AddElementByElement(fourthVector);
            Console.Write("Sum of third vector and fourth vector: ");
            foreach(var vector in sixthVector)
            {
                Console.Write($"{vector.ToString()} ");
            }
            var seventhVector = thirdVector.SubtractElementByElement(fourthVector);
            Console.Write("\nSubtraction of third vector and fourth vector: ");
            foreach (var vector in seventhVector)
            {
                Console.Write($"{vector.ToString()} ");
            }
            var eighthVector = firstVector.MultiplyByScalar(3);
            Console.Write("\nFirst vector, multiplied by 3: ");
            foreach (var vector in eighthVector)
            {
                Console.Write($"{vector.ToString()} ");
            }
            Console.WriteLine();
            
            if(firstVector.CompareTo(secondVector)>0)
                Console.WriteLine("First vector is longer than second");
            else if(firstVector.CompareTo(secondVector)<0)
                Console.WriteLine("Second vector is longer than first");
            else
                Console.WriteLine("Vectors have equal length");
            Console.ReadKey();
        }
    }
}
