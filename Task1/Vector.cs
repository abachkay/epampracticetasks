﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task1
{
    /// <summary>
    /// Represents a vector of integer values. Provides methods to add vectors' elements one by one, substract, multyply by a scalar and compare
    /// </summary>
    class Vector : IComparable<Vector>,IEnumerable<int>
    {
        private int[] _array;
        private int _leftBoundary = 0;

        public int this[int i]
        {
            get
            {                
               return _array[i - _leftBoundary];
             
            }
            set
            {
                _array[i - _leftBoundary] = value;
            }
                
        }
        public int Length
        {
            get
            {
                return _array.Length;
            }
        }
        public int LeftBoundary
        {
            get
            {
                return _leftBoundary;
            }
        }
        public int RightBoundary
        {
            get
            {
                return _leftBoundary+_array.Length-1;
            }
        }
        /// <summary>
        /// Initializes a new instance of Vector that is empty and has zero start index.
        /// </summary>
        /// <param name="length">Length of vector</param>
        public Vector(int length)
        {
            _array = new int[length];
        }
        /// <summary>
        /// Initializes a new instance of Vector that is empty.
        /// </summary>
        /// <param name="leftBoundary"> Start index of vector. </param>
        /// <param name="rightBoundary"> Index of last element in vector </param>
        public Vector(int leftBoundary, int rightBoundary)
        {
            _leftBoundary = leftBoundary;
            _array = new int[rightBoundary - leftBoundary + 1];
        }
        public Vector AddElementByElement(Vector otherVector)
        {
            if (_leftBoundary != otherVector._leftBoundary || _array.Length != otherVector._array.Length)
            {
                throw new ArgumentException("Vectors' index boundaries are not matching");
            }
            var resultVector = new Vector(_leftBoundary, _leftBoundary + _array.Length - 1);
            for (int i = _leftBoundary; i < _leftBoundary + _array.Length; i++)
            {
                resultVector[i] = this[i] + otherVector[i];
            }
            return resultVector;
        }
        public Vector SubtractElementByElement(Vector otherVector)
        {
            if (_leftBoundary != otherVector._leftBoundary || _array.Length != otherVector._array.Length)
            {
                throw new ArgumentException("Vectors' index boundaries are not matching");
            }
            var resultVector = new Vector(_leftBoundary, _leftBoundary + _array.Length - 1);
            for (int i = _leftBoundary; i < _leftBoundary + _array.Length; i++)
            {
                resultVector[i] = this[i] - otherVector[i];
            }
            return resultVector;
        }
        public Vector MultiplyByScalar(int scalar)
        {
            var resultVector = new Vector(_leftBoundary, _leftBoundary + _array.Length - 1);
            for (int i = _leftBoundary; i < _leftBoundary + _array.Length; i++)
            {
                resultVector[i] = this[i] * scalar;
            }
            return resultVector;
        }
        public int CompareTo(Vector other)
        {
            if (other == null)
            {
                throw new ArgumentNullException();
            }
            return _array.Length < other._array.Length ? -1 : _array.Length > other._array.Length ? 1 : 0;
        }

        public IEnumerator<int> GetEnumerator()
        {
            for (int i = 0; i < _array.Length; i++)
                yield return _array[i];
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
