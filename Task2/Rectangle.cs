﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task2
{
    /// <summary>
    /// Represents a rectangle with sides parallel to axes
    /// </summary>
    class Rectangle
    {
        private double _x1,_y1,_x2,_y2;

        public double X1
        {
            get
            {
                return _x1;
            }
            set
            {
                _x1 = value;
            }
        }
        public double Y1
        {
            get
            {
                return _y1;
            }
            set
            {
                _y1 = value;
            }
        }
        public double X2
        {
            get
            {
                return _x2;
            }
            set
            {
                _x2 = value;
            }
        }
        public double Y2
        {
            get
            {
                return _y2;
            }
            set
            {
                _y2 = value;
            }
        }

        /// <summary>
        /// Creates a rectangle by left top point and right bottom point
        /// </summary>
        public Rectangle(double x1,  double y1,double x2, double y2)
        {
            _x1 = x1;
            _y1 = y1;
            _x2 = x2;
            _y2 = y2;
        }
        public void Move(double horizontalShift, double verticalShift)
        {
            _x1 += horizontalShift;
            _x2 += horizontalShift;
            _y1 += verticalShift;
            _y2 += verticalShift;
        }
        public void Scale(double horizontalScaler, double verticalScaler)
        {
            _x1 *= horizontalScaler;
            _x2 *= horizontalScaler;
            _y1 *= verticalScaler;
            _y2 *= verticalScaler;
        }
        /// <summary>
        /// Creates new minimum size Rectangle that contains two others.
        /// </summary>
        public Rectangle GetWrappingRectangle(Rectangle otherRect)
        {
           
            double x1 = Math.Min(_x1, otherRect._x1);
            double y1 = Math.Min(_y1, otherRect._y1);
            double x2 = Math.Max(_x2, otherRect._x2);
            double y2 = Math.Max(_y2, otherRect._y2);           
            return new Rectangle(x1, y1, x2, y2);
        }
        /// <summary>
        /// Creates new Rectangle which is an intersection of two others
        /// </summary>     
        public Rectangle GetIntersectionRectangle(Rectangle otherRect)
        {
            if (_x1 >= otherRect._x2 || _x2 <= otherRect._x1 || _y1 <= otherRect._y2 || _y2 >= otherRect._y1)
                throw new ArgumentException("Rectangles do not intersect");
            double x1 = Math.Max(_x1, otherRect._x1);
            double y1 = Math.Min(_y1, otherRect._y1);
            double x2 = Math.Min(_x2, otherRect._x2);
            double y2 = Math.Max(_y2, otherRect._y2);
            return new Rectangle(x1, y1, x2, y2);
        }
    }
}
