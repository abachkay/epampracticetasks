﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            var firstPoly = new Polynom(2, new double[] { 1, 1, 1 });
            Console.WriteLine("First polynom:");
            PrintPoly(firstPoly);
            Console.WriteLine($"First polynom calculation result (x=2): {firstPoly.Calculate(2)}");
            var secondPoly = new Polynom(3, new double[] { 2, 2, 2 ,2});
            Console.WriteLine("Second polynom:");
            PrintPoly(secondPoly);
            var resultPoly = firstPoly.Add(secondPoly);
            Console.WriteLine($"Addition result polynom:");
            PrintPoly(resultPoly);
            resultPoly = firstPoly.Subtract(secondPoly);
            Console.WriteLine($"Subtraction result polynom:");
            PrintPoly(resultPoly);
            resultPoly = firstPoly.MultiplyByScalar(4);
            Console.WriteLine($"Multiplication of first polynom by 4 result:");
            PrintPoly(resultPoly);
            Console.ReadKey();
        }
        public static void PrintPoly(Polynom poly)
        {
            for (int i = 0; i < poly.Power+1; i++)
            {
                Console.Write($"{poly.Coefs[i]}*x^{i}");
                if (i != poly.Power)
                {
                    Console.Write(" + ");
                }              
            }
            Console.WriteLine();
        }
    }
}
