﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3
{
    /// <summary>
    /// Represents polynom, powers ordered ascending.
    /// </summary>
    class Polynom
    {
        private double[] _coefs;
        private int _power = 0;

        public int Power
        {
            get
            {
                return _power;
            }
        }

        public double[] Coefs
        {
            get
            {
                return _coefs;
            }
        }
        /// <summary>
        /// Creates an instanece of Polynom, coefs are oredered ascending
        /// </summary>        
        public Polynom(int power, double[] coefs)
        {
            if (power != coefs.Length - 1)
            {
                throw new ArgumentException("Coefficients number should be power-1");
            }
            _coefs = coefs;
            _power = power;
        }
        public double Calculate(double x)
        {
            double res = 0;
            for(int i=0;i<_power+1;i++)
            {
                res += _coefs[i] * Math.Pow(x, i+1);
            }
            return res;
        }
        public Polynom Add(Polynom otherPolynom)
        {
            var power = Math.Max(_power, otherPolynom._power);
            var coefs = new double[power+1];
            for (int i = 0; i < Math.Min(_power, otherPolynom._power)+1; i++)
            {
                coefs[i] = _coefs[i] + otherPolynom._coefs[i];
            }
            for (int i = Math.Min(_power, otherPolynom._power)+1; i < power+1; i++)
            {
                if (_power > otherPolynom._power)
                {
                    coefs[i] = _coefs[i];
                }
                else
                {
                    coefs[i] = otherPolynom._coefs[i];
                }
            }
            return new Polynom(power,coefs);
        }
        public Polynom Subtract(Polynom otherPolynom)
        {
            var power = Math.Max(_power, otherPolynom._power);
            var coefs = new double[power+1];
            for (int i = 0; i < Math.Min(_power, otherPolynom._power)+1; i++)
            {
                coefs[i] = _coefs[i] - otherPolynom._coefs[i];
            }
            for (int i = Math.Min(_power, otherPolynom._power)+1; i < power+1; i++)
            {
                if (_power > otherPolynom._power)
                {
                    coefs[i] = _coefs[i];
                }
                else
                {
                    coefs[i] = -otherPolynom._coefs[i];
                }
            }
            return new Polynom(power, coefs);
        }
        public Polynom MultiplyByScalar(double scalar)
        {
            var coefs = new double[_power + 1];
            for(int i=0;i<coefs.Length;i++)
            {
                coefs[i] = _coefs[i] * scalar;
            }
            return new Polynom(_power, coefs);
        }        
    }
}
